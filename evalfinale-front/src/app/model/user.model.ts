export class UserModel {

  id: string;
  pseudo: string;
  mail: string;


  constructor(id: string, pseudo: string, mail: string) {
    this.id = id;
    this.pseudo = pseudo;
    this.mail = mail;
  }
}
