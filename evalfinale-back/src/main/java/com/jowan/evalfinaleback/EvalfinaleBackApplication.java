package com.jowan.evalfinaleback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvalfinaleBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvalfinaleBackApplication.class, args);
	}

}
