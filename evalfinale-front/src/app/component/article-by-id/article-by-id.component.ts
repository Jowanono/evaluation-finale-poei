import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-article-by-id',
  templateUrl: './article-by-id.component.html',
  styleUrls: ['./article-by-id.component.css']
})
export class ArticleByIdComponent implements OnInit {
  idFromInput: string;
  // tslint:disable-next-line:no-inferrable-types
  isHidden: boolean = true;
  error: string;

  constructor() {
  }

  ngOnInit() {
  }

  change() {
    this.isHidden = true;
  }

  clicked() {
    this.isHidden = false;
  }

  onArticleError($event) {
    this.error = $event;
  }

}
