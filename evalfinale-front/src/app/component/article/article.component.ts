import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ArticleModel} from '../../model/article.model';
import {ZenikademyService} from '../../service/zenikademy.service';
import {ActivatedRoute} from '@angular/router';
import {ArticleToPublishModel} from '../../model/articleToPublish.model';
import {ArticleToUpdateModel} from '../../model/articleToUpdate.model';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit, OnChanges {

  article: ArticleModel;
  @Input() id: string;
  // tslint:disable-next-line:no-inferrable-types
  isHidden: boolean = true;
  // tslint:disable-next-line:no-inferrable-types
  buttonIsHidden: boolean = false;
  @Output() errorArticle = new EventEmitter<string>();
  articleToPublish: ArticleToPublishModel;
  updatedContentFromInput: string;

  constructor(
    private service: ZenikademyService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.id = params.get('id');
      }
      console.log(this.id);
      this.getArticle();
    });
  }

  ngOnChanges() {
    this.getArticle();
    this.publish();
    this.update();
  }

  getArticle() {
    if (this.id) {
      this.service.getAnArticleById(this.id).subscribe(a => {
        console.log(a);
        this.article = a;
      }, error => {
        console.log(error);
        if (error.status === 404) {
          this.errorArticle.emit('Cet article n\'existe pas.');
          console.log(error.status);
        }
      });
    }
  }

  publish() {
    this.service.updateArticleStatus(this.id, this.articleToPublish).subscribe(a => {
      console.log(a);
      this.getArticle();
    });

  }

  update() {
    const articleToUpdate = new ArticleToUpdateModel(this.updatedContentFromInput);
    this.service.updateArticle(this.id, articleToUpdate).subscribe(a => {
      console.log(a);
      this.getArticle();
      this.isHidden = true;
    });

  }

  change() {
    this.isHidden = false;
  }
}

