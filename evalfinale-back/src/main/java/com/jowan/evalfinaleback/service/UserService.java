package com.jowan.evalfinaleback.service;

import com.jowan.evalfinaleback.controller.userRepresentation.NewUserRepresentation;
import com.jowan.evalfinaleback.domain.Article;
import com.jowan.evalfinaleback.domain.User;
import com.jowan.evalfinaleback.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserService {

    UserRepository userRepository;
    IdGenerator idGenerator;

    @Autowired
    public UserService(UserRepository userRepository, IdGenerator idGenerator) {
        this.userRepository = userRepository;
        this.idGenerator = idGenerator;
    }

    public List<User> getAllUsers() {
        return this.userRepository.findAll();
    }

    public Optional<User> getOneUserById(String id) {
        return this.userRepository.findById(id);
    }

    public User addAUser(NewUserRepresentation u) {
        User user = new User(this.idGenerator.generateNewId(), u.getPseudo(), u.getMail());
        this.userRepository.save(user);
        return user;
    }

    public void deleteById(String id) {
        this.userRepository.deleteById(id);
    }

}


