DROP TABLE if exists articles;
DROP TABLE if exists users;


CREATE TABLE users
(
    id     text PRIMARY KEY,
    pseudo text not null,
    mail   text not null
);

CREATE TABLE articles
(
    id       text PRIMARY KEY,
    userId   text REFERENCES users (id),
    title    text not null,
    content  text not null,
    synopsis text,
    status   text,
    category text
);

SELECT * FROM articles;

SELECT a FROM articles a where a.category = '5';
