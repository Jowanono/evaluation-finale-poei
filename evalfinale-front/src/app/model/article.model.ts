export class ArticleModel {

  id: string;
  title: string;
  content: string;
  synopsis: string;
  status: string;
  category: string;


  constructor(id: string, title: string, content: string, synopsis: string, status: string, category: string) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.synopsis = synopsis;
    this.status = status;
    this.category = category;
  }
}
