export class ArticleToCreateModel {

  title: string;
  content: string;
  synopsis: string;
  category: string;


  constructor(title: string, content: string, synopsis: string, category: string) {
    this.title = title;
    this.content = content;
    this.synopsis = synopsis;
    this.category = category;
  }
}
