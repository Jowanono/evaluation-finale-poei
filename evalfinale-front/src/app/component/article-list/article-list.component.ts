import {Component, Input, OnInit} from '@angular/core';
import {ArticleModel} from '../../model/article.model';
import {ZenikademyService} from '../../service/zenikademy.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {
  @Input() articleList: ArticleModel [] = [];

  constructor(private service: ZenikademyService) { }

  ngOnInit() {
    this.service.getArticleList().subscribe(aList => {
      console.log(aList);
      this.articleList = aList;
    });
  }

}
