import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilCreatorComponent } from './profil-creator.component';

describe('ProfilCreatorComponent', () => {
  let component: ProfilCreatorComponent;
  let fixture: ComponentFixture<ProfilCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
