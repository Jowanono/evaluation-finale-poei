package com.jowan.evalfinaleback.domain.articleEnum;

public enum ArticleCategory {

    BACKEND("Backend", 0),
    FRONTEND("Frontend", 1),
    DATA("Data", 2),
    SECURITY("Securité", 3),
    CICD("Intégration continue", 4),
    CHITCHAT("Divers", 5);

    private final String value;
    private final int number;

    ArticleCategory(final String newValue, final int newNumber){

        this.value = newValue;
        this.number = newNumber;
    }

    public String getValue(){
        return value;
    }

    public int getNumber() {
        return number;
    }
}
