package com.jowan.evalfinaleback.repository;

import com.jowan.evalfinaleback.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, String> {

    List<User> findAll();
}
