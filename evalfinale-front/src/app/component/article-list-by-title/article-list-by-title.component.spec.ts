import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleListByTitleComponent } from './article-list-by-title.component';

describe('ArticleListByTitleComponent', () => {
  let component: ArticleListByTitleComponent;
  let fixture: ComponentFixture<ArticleListByTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleListByTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleListByTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
