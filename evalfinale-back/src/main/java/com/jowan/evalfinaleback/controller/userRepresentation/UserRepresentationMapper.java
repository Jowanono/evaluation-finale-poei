package com.jowan.evalfinaleback.controller.userRepresentation;

import com.jowan.evalfinaleback.domain.User;
import org.springframework.stereotype.Component;

@Component
public class UserRepresentationMapper {

    public DisplayableUserRepresentation mapToDisplayableUser(User u) {
        DisplayableUserRepresentation result = new DisplayableUserRepresentation();
        result.setId(u.getId());
        result.setPseudo(u.getPseudo());
        result.setMail(u.getMail());

        return result;
    }
}
