import {Component, Input, OnInit} from '@angular/core';
import {ZenikademyService} from '../../service/zenikademy.service';
import {ArticleToCreateModel} from '../../model/articleToCreate.model';
import {UserModel} from '../../model/user.model';
import {UserToCreateModel} from '../../model/userToCreate.model';

@Component({
  selector: 'app-profil-creator',
  templateUrl: './profil-creator.component.html',
  styleUrls: ['./profil-creator.component.css']
})
export class ProfilCreatorComponent implements OnInit {

  @Input() pseudo: string;
  @Input() mail: string;
  creationValidation: string;

  constructor(private service: ZenikademyService) {
  }

  ngOnInit() {
  }

  createProfil() {
    const userToCreate = new UserToCreateModel(this.pseudo, this.mail);
    this.service.createProfil(userToCreate).subscribe(a => {
      console.log(a);
      this.creationValidation = `Votre profil a bien été crée! Bravo et Bienvenue!`;
    });
  }

}
