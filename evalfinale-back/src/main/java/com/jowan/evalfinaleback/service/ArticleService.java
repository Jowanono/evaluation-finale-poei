package com.jowan.evalfinaleback.service;

import com.jowan.evalfinaleback.controller.articleRepresentation.NewArticleRepresentation;
import com.jowan.evalfinaleback.domain.Article;
import com.jowan.evalfinaleback.domain.articleEnum.ArticleCategory;
import com.jowan.evalfinaleback.domain.articleEnum.ArticleStatus;
import com.jowan.evalfinaleback.repository.ArticleRepository;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ArticleService {

    private ArticleRepository articleRepository;
    private IdGenerator idGenerator;
    final LevenshteinDistance d = new LevenshteinDistance();

    @Autowired

    public ArticleService(ArticleRepository articleRepository, IdGenerator idGenerator) {
        this.articleRepository = articleRepository;
        this.idGenerator = idGenerator;
    }

    public List<Article> getAllArticles(){
        return this.articleRepository.findAll();
    }

    public Optional<Article> getOneArticleById(String id){
        return this.articleRepository.findById(id);
    }

    public long determineThreshold(String expectedTitle) {
        double MAX_DISTANCE_DIFF_RATIO = 0.3;
        return Math.max(Math.round(MAX_DISTANCE_DIFF_RATIO * expectedTitle.length()), 1);
    }

    public boolean tryTitle(String expectedTitle, String userTitle) {
        return ((d.apply(expectedTitle.toLowerCase(), userTitle.toLowerCase())) < determineThreshold(userTitle));
    }

    public List<Article> getAllArticlesByTitle(String title) {
        return this.articleRepository.fetchArticlesByTitle(title);
    }

    /*public List<Article> getAllArticlesByTitle(String userTitle) throws Exception{
        List<Article> articles = this.articleRepository.findAll();
        for(Article a : articles) {
            if(this.tryTitle(a.getTitle(), userTitle)) {
                return this.articleRepository.fetchArticlesByTitle(a.getTitle());
            }
        } throw new Exception("Ce Titre n'appartient à aucun article");

    }*/

    public List<Article> getAllArticlesByCategory(ArticleCategory category) {
        return this.articleRepository.fetchArticlesByCategory(category);
    }

    public Article addAnArticle (NewArticleRepresentation a){
        Article article = new Article(this.idGenerator.generateNewId(), a.getTitle(),
                a.getContent(), a.getSynopsis(), ArticleStatus.DRAFT, a.getCategory());
        this.articleRepository.save(article);
        return article;
    }

    public void deleteById(String id){
        this.articleRepository.deleteById(id);
    }

    public Article modifyAnArticle(String id, NewArticleRepresentation a){
        Article articleToChange = this.articleRepository.findById(id).get();
        Article articleChangedByUser = new Article(id, a.getTitle(), a.getContent(),
                a.getSynopsis(), ArticleStatus.DRAFT, a.getCategory());
        String newContent;

        if(articleChangedByUser.getContent().equals("")){
            newContent = articleToChange.getContent();
        } else {
            newContent = articleChangedByUser.getContent();
        }

        Article finalArticle = new Article(id, articleToChange.getTitle(), newContent, articleToChange.getSynopsis(),
                ArticleStatus.DRAFT, articleToChange.getCategory());

        this.articleRepository.save(finalArticle);

        return finalArticle;
    }

    public Article publishAnArticle(Article a) {
        Article  articleToPublish  = new Article(a.getId(), a.getTitle(), a.getContent(), a.getSynopsis(),
                ArticleStatus.PUBLISHED, a.getCategory());
        this.articleRepository.save(articleToPublish);
        return articleToPublish;
    }
}
