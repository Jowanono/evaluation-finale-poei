import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ArticleComponent} from './component/article/article.component';
import {ArticleListComponent} from './component/article-list/article-list.component';
import {AccueilComponent} from './component/accueil/accueil.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ZenikademyService} from './service/zenikademy.service';
import { ArticleByIdComponent } from './component/article-by-id/article-by-id.component';
import { ArticleGeneratorComponent } from './component/article-generator/article-generator.component';
import { ArticleFinderComponent } from './component/article-finder/article-finder.component';
import { ArticleByTitleComponent } from './component/article-by-title/article-by-title.component';
import { ArticleListByTitleComponent } from './component/article-list-by-title/article-list-by-title.component';
import { ProfilCreatorComponent } from './component/profil-creator/profil-creator.component';
import { StatsComponent } from './component/stats/stats.component';
import { PieChartComponent } from './component/pie-chart/pie-chart.component';

const appRoutes: Routes = [
  {path: 'articleList', component: ArticleListComponent},
  {path: 'articleFinder', component: ArticleFinderComponent},
  {path: 'article/:id', component: ArticleComponent},
  {path: 'article', component: ArticleComponent},
  {path: 'articleGenerator', component: ArticleGeneratorComponent},
  {path: 'profilCreator', component: ProfilCreatorComponent},
  {path: 'stats', component: StatsComponent},
  {path: '', component: AccueilComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    ArticleListComponent,
    AccueilComponent,
    ArticleByIdComponent,
    ArticleGeneratorComponent,
    ArticleFinderComponent,
    ArticleByTitleComponent,
    ArticleListByTitleComponent,
    ProfilCreatorComponent,
    StatsComponent,
    PieChartComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    HttpClientModule,
    RouterModule,
    FormsModule
  ],
  providers: [
    ZenikademyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
