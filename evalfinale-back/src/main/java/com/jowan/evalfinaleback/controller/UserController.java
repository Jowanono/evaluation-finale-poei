package com.jowan.evalfinaleback.controller;


import com.jowan.evalfinaleback.controller.userRepresentation.DisplayableUserRepresentation;
import com.jowan.evalfinaleback.controller.userRepresentation.NewUserRepresentation;
import com.jowan.evalfinaleback.controller.userRepresentation.UserRepresentationMapper;
import com.jowan.evalfinaleback.domain.User;
import com.jowan.evalfinaleback.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/Zenikademy")
public class UserController {

    private UserService userService;
    private UserRepresentationMapper mapper;

    @Autowired
    public UserController(UserService userService, UserRepresentationMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping("/users")
    List<DisplayableUserRepresentation> getAllUsers() {
        return this.userService.getAllUsers().stream()
                .map(this.mapper::mapToDisplayableUser)
                .collect(Collectors.toList());
    }

    @GetMapping("/users/{id}")
    ResponseEntity<DisplayableUserRepresentation> getOneUserById(@PathVariable("id") String id) {
        Optional<User> optionalUser = this.userService.getOneUserById(id);

        return optionalUser
                .map(this.mapper::mapToDisplayableUser)
                .map(ResponseEntity::ok)
                .orElseGet(()-> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/users/{id}")
    void deleteUserById (@PathVariable("id") String id) {
        this.userService.deleteById(id);

    }

    @PostMapping("/users")
    User createUser(@RequestBody NewUserRepresentation userRepresentation) {
        return this.userService.addAUser(userRepresentation);

    }
}
