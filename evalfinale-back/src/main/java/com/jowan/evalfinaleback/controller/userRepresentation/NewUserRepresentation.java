package com.jowan.evalfinaleback.controller.userRepresentation;

import com.jowan.evalfinaleback.domain.Article;

import java.util.Set;

public class NewUserRepresentation {

    private String pseudo;
    private String mail;

    public NewUserRepresentation(String pseudo, String mail) {
        this.pseudo = pseudo;
        this.mail = mail;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
