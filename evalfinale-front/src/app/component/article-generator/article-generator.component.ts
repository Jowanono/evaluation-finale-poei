import {Component, Input, OnInit} from '@angular/core';
import {ZenikademyService} from '../../service/zenikademy.service';
import {ArticleToCreateModel} from '../../model/articleToCreate.model';

@Component({
  selector: 'app-article-generator',
  templateUrl: './article-generator.component.html',
  styleUrls: ['./article-generator.component.css']
})
export class ArticleGeneratorComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  @Input() synopsis: string;
  @Input() category: string;
  creationValidation: string;

  constructor(private service: ZenikademyService) {
  }

  ngOnInit() {
  }

  generateArticle() {
    const article = new ArticleToCreateModel(this.title, this.content, this.synopsis, this.category);
    this.service.createArticle(article).subscribe(a => {
      console.log(a);
      this.creationValidation = `Votre article a bien été généré! Retrouvez le dans l'onglet 'Tous les articles'`;
    });
  }

}
