import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ArticleModel} from '../../model/article.model';
import {ZenikademyService} from '../../service/zenikademy.service';

@Component({
  selector: 'app-article-list-by-title',
  templateUrl: './article-list-by-title.component.html',
  styleUrls: ['./article-list-by-title.component.css']
})
export class ArticleListByTitleComponent implements OnInit {

  @Input() articleListByTitle: ArticleModel [] = [];
  @Input() title: string;

  constructor(private service: ZenikademyService) { }

  ngOnInit() {
    this.service.getArticleListByTitle(this.title).subscribe(aList => {
      console.log(aList);
      this.articleListByTitle = aList;
    });
  }

}
