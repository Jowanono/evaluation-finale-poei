# Blog de la Zenika Academy

La Zenika Academy souhaite développer un outil personnalisé pour publier les articles des anciens élèves à destination des futures promotions.

Il est recommandé de lire l'intégralité de ce document avant de démarrer les développements.

## Description générale de l'application

L'outil présentera des fonctionnalités similaires à une platforme de Blogging mais on ne souhaite pas réutiliser des outils existants (type Wordpress) qui sont beaucoup trop complexes pour l'usage attendu.

L'application devra permettre à des utilisateurs de publier des articles. Les articles sont ensuite disponibles pour tous les visiteurs, même s'ils n'ont pas de compte utilisateur. 

Afin de faciliter la découvert de nouveaux articles, des systèmes de catégories et de tags sont à prévoir sur les articles. On envisage aussi un système de statistiques sur les articles et les utilisateurs pour mieux connaitre le contenu du blog.

## Cahier des charges fonctionnel

Le cachier des charges est découpé en différents "Chantiers" regroupant plusieurs fonctionnalités qui partagent un même "thème". 

Il n'est pas nécéssaire de finir toutes les fonctionnalités d'un chantier avant de passer au suivant, vous êtes libres de choisir l'ordre dans lequel vous abordez ces différents chantier (même si certains chantiers nécéssitent des fonctionnalités pour pouvoir être codés). 

En revanche, au sein d'un chantier, les fonctionnalités sont classés par ordre grossier de priorité.

Dans ce cahier des charges, on parle de plusieurs catégories de personnes : 
 - Un « Visiteur » est une personne anonyme qui consulte le blog et qui visualise des articles ou d'autres pages.
 - Un « Utilisateur » est une personne qui écrit des articles ou qui crée d'autres entités. Il pourra à terme être authentifié par son pseudonyme et son adresse mail.
 
 Pour simplifier les phrases, on considère qu'un utilisateur est aussi un visiteur (il peut faire tout ce que peut faire un visiteur).

### Chantier « Écriture et affichage des articles »

Un article contient les données suivantes : 
 * Un titre ;
 * Un contenu ;
 * Un résumé.
Le résumé est une courte phrase d'accroche qui est affichée pour donner envie aux visiteurs de lire l'article. Il ne doit pas dépasser les 50 mots.

#### Fonctionnalités principales
 * Un utilisateur peut rédiger un article
 * Un visiteur peut afficher la liste des articles (titre + accroche)
 * Un visiteur peut afficher ledétail d'un article (Titre, accroche, contenu)

#### Fonctionnalités « Nice to have »
 * Un utilisateur peut modifier un article
 * Quand un article est créé, il est en status `draft` (brouillon). Les brouillons ne sont pas affichés dans la liste des articles, mais il est quand même possible d'y accéder si on connait leur ID (dans ce cas, on affiche un bandeau en haut de la page pour avertir qu'on visualise un brouillon) 
 * Publier un article : un article draft peut être publié par un utilisateur. Son statut change de `draft` à `published`
 * Pour des raisons de référencement, le titre et l'accroche d'un article peuvent plus être modifiés une fois que l'article est publié.
 
### Chantier « Authentification des utilisateurs »

On souhaite enregistrer les informations sur les auteurs des articles, ainsi que gérer quelques permissions.

Lorsqu'un article est créé, on enregistre l'auteur pour référence. L'auteur d'un article ne peut pas être modifié.

Pour un utilisateur, les informations stockées sont les suivantes : 
 * Un pseudonyme
 * Une adresse mail

#### Fonctionnalités principales

 * Pouvoir créer des utilisateurs
 * Mémoriser l'auteur d'un article. Les articles qui n'ont pas d'auteur doivent être rattachés à un utilisateur fictif qui s'appelle "Auteur inconnu" et qui a pour adresse mail `anonymous@anonymous`
 
#### Fonctionnalités « nice to have »

 * Un utilisateur authentifié peut voir la liste de ses drafts en cours de rédaction (et reprendre la rédaction, puis publier)
 * Un visiteur peut voir la liste des articles d'un utilisateur donné
 * Un utilisateur ne peut pas modifier un article écrit par quelqu'un d'autre
 
 
### Chantier « Recherche et Navigation »

L'objectif à moyen terme est d'héberger un grand nombre d'articles sur la plateforme. En conséquence, on souhaite fournir un système de classification des articles pour faciliter la découvert de nouveau contenu.

On souhaite aussi fournir un système de recherche pour retrouver facilement un article préalablement consulté.

#### Fonctionnalités principales

 * Un visiteur peut rechercher un article via son titre. La recherche doit trouver les articles dont le titre ne correspond que partiellement ou si la recherche contient des fautes de frappes.
 * Les articles sont classés par catégories. La liste des catégories est paramétrée préalablement à la création des articles.
 * Un visiteur peut visualiser la liste des articles d'une catégorie donnée.
 
#### Fonctionnalités « Nice to have »

 * Les articles peuvent avoir une liste de "tags". Contrairement aux catégories, un article peut avoir plusieurs tags et les tags peuvent être créés en même temps que l'article.
 * Un visiteur peut visualiser la liste des articles d'un tag donné.
 
### Chantier « Statistiques »

Toujours dans l'objectif d'héberger beaucoup de contenu sur la plateforme, on souhaite avoir un certain nombre d'informations statistiques sur les articles postés sur la plateforme (nombre de mots moyen des articles, nombre moyen d'articles par utilisateurs, ...)

 * Créer un écran de statistiques présentant les statistiques suivantes : 
    * Nombre d'articles
    * Nombre de tags par article (et moyenne)
    * Nombre de mots par article (et moyenne)
 * Toutes ces statistiques peuvent être affichées pour une catégorie donnée, un tag donné ou un auteur donné.
 
 
## Critères d'évaluation

### Notation

Comme d'habitude, on privilégie une application avec un peu moins de fonctionnalités mais qui fonctionne correctement plutôt qu'une application avec beaucoup de fonctionnalités non terminées.

La répartition de la notation est grossièrement de : 
 * 50% pour la partie Java/Spring/SQL (backend)
 * 30% pour la partie Javascript/Angular (frontend)
 * 20% pour l'intégration continue, les livrables et les tests (Junit, Docker, gitlab CI)
 
Il n'y a pas de barême précis pour chaque tâche. Le cahier des charges est volontairement trop long pour la durée de l'évaluation, ce qui vous permet (en partie) de choisir sur quelles fonctionnalités travailler.

### Soutenance

Le dernier jour, vous présenterez votre application devant 4 développeurs. Le format de la soutenance est libre, l'objectif étant de mettre en valeur au mieux le travail fourni.

L'idéal est de montrer l'application finale en fonctionnement (via le front ou l'API Rest si elle présente des fonctionnalités supplémentaires) mais vous pouvez également montrer : 
   * Un diagramme de classes
   * Un modèle conceptuel de données
   * Le script SQL de création de la base de données
   * Une partie du code de votre application qui vous semble particulièrement intéressant ou qui a demandé beaucoup d'effort.
   
 Pensez également à montrer :
  * La pipeline d'intégration continue
  * La façon dont vous construisez et démarrez votre application (avec maven, avec docker...) 
  * Le résultat des tests unitaires