package com.jowan.evalfinaleback.controller.articleRepresentation;

import com.jowan.evalfinaleback.domain.articleEnum.ArticleCategory;

public class NewArticleRepresentation {

    private String title;
    private String content;
    private String synopsis;
    private ArticleCategory category;

    public NewArticleRepresentation(String title, String content, String synopsis, ArticleCategory category) {
        this.title = title;
        this.content = content;
        this.synopsis = synopsis;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public ArticleCategory getCategory() {
        return category;
    }
}
