package com.jowan.evalfinaleback.controller;

import com.jowan.evalfinaleback.controller.articleRepresentation.ArticleRepresentationMapper;
import com.jowan.evalfinaleback.controller.articleRepresentation.DiplayableArticleRepresentation;
import com.jowan.evalfinaleback.controller.articleRepresentation.NewArticleRepresentation;
import com.jowan.evalfinaleback.domain.Article;
import com.jowan.evalfinaleback.domain.articleEnum.ArticleCategory;
import com.jowan.evalfinaleback.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/Zenikademy")
public class ArticleController {

    private ArticleService articleService;
    private ArticleRepresentationMapper mapper;

    @Autowired
    public ArticleController(ArticleService articleService, ArticleRepresentationMapper mapper) {
        this.articleService = articleService;
        this.mapper = mapper;
    }

    @GetMapping("/articles")
    List<DiplayableArticleRepresentation> getAllArticles() {
        return this.articleService.getAllArticles().stream()
                .map(this.mapper::mapToDisplayableArticle)
                .collect(Collectors.toList());
    }

    @GetMapping("/articles/title/{titre}")
    List<DiplayableArticleRepresentation> getAllArticlesByTitle(@PathVariable("titre") String titre) {
        return this.articleService.getAllArticlesByTitle(titre).stream()
                .map(this.mapper::mapToDisplayableArticle)
                .collect(Collectors.toList());
    }

    @GetMapping("/articles/category/{category}")
    List<DiplayableArticleRepresentation> getAllArticlesByCategory(@PathVariable("category") ArticleCategory category) {
        return this.articleService.getAllArticlesByCategory(category).stream()
                .map(this.mapper::mapToDisplayableArticle)
                .collect(Collectors.toList());
    }

    @GetMapping("/articles/{id}")
    ResponseEntity<DiplayableArticleRepresentation> getOneArticleById(@PathVariable("id") String id) {
        Optional<Article> optionalArticle = this.articleService.getOneArticleById(id);

        return optionalArticle
                .map(this.mapper::mapToDisplayableArticle)
                .map(ResponseEntity::ok)
                .orElseGet(()-> ResponseEntity.notFound().build());
    }

    @PostMapping("/articles")
    Article createAnArticle(@RequestBody NewArticleRepresentation articleRepresentation){
        return this.articleService.addAnArticle(articleRepresentation);
    }

    @PutMapping("/articles/{id}")
    ResponseEntity<DiplayableArticleRepresentation> modifyAnArticle(@PathVariable("id") String id,
                                                                    @RequestBody NewArticleRepresentation articleRepresentation){
        this.articleService.modifyAnArticle(id, articleRepresentation);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/articles/publish/{id}")
    Article modifyAnArticle(@PathVariable("id") String id) {
        Article articleToPublish = this.articleService.getOneArticleById(id).get();
        return this.articleService.publishAnArticle(articleToPublish);
    }

    @DeleteMapping("/articles/{id}")
    void deleteAnArticleById (@PathVariable("id") String id){
        this.articleService.deleteById(id);
    }

}
