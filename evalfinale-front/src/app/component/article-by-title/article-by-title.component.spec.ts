import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleByTitleComponent } from './article-by-title.component';

describe('ArticleByTitleComponent', () => {
  let component: ArticleByTitleComponent;
  let fixture: ComponentFixture<ArticleByTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleByTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleByTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
