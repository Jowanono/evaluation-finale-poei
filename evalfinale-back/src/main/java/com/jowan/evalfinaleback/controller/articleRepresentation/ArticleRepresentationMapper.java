package com.jowan.evalfinaleback.controller.articleRepresentation;

import com.jowan.evalfinaleback.domain.Article;
import org.springframework.stereotype.Component;

@Component
public class ArticleRepresentationMapper {

    public DiplayableArticleRepresentation mapToDisplayableArticle(Article a) {

        DiplayableArticleRepresentation result = new DiplayableArticleRepresentation();

        result.setId(a.getId());
        result.setTitle(a.getTitle());
        result.setContent(a.getContent());
        result.setSynopsis(a.getSynopsis());
        result.setStatus(a.getStatus().getValue());
        result.setCategory(a.getCategory().getValue());

        return result;
    }


}
