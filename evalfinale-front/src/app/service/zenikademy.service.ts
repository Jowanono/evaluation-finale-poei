import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ArticleModel} from '../model/article.model';
import {ArticleToCreateModel} from '../model/articleToCreate.model';
import {ArticleToUpdateModel} from '../model/articleToUpdate.model';
import {ArticleToPublishModel} from '../model/articleToPublish.model';
import {UserToCreateModel} from '../model/userToCreate.model';
import {UserModel} from '../model/user.model';

@Injectable()
export class ZenikademyService {


  constructor(private http: HttpClient) {
  }

  getAnArticleById(id: string): Observable<ArticleModel> {
    return this.http.get<ArticleModel>('http://localhost:8080/Zenikademy/articles/' + id);
  }

  getArticleList(): Observable<ArticleModel []> {
    return this.http.get<ArticleModel []>('http://localhost:8080/Zenikademy/articles');
  }

  getUserList(): Observable<UserModel []> {
    return this.http.get<UserModel []>('http://localhost:8080/Zenikademy/users');
  }

  getArticleListByTitle(title: string): Observable<ArticleModel []> {
    return this.http.get<ArticleModel []>('http://localhost:8080/Zenikademy/articles/title/' + title);
  }

  createArticle(articleToCreate: ArticleToCreateModel) {
    return this.http.post('http://localhost:8080/Zenikademy/articles', articleToCreate);
  }

  createProfil(userToCreate: UserToCreateModel) {
    return this.http.post('http://localhost:8080/Zenikademy/users', userToCreate);
  }

  updateArticle(id: string, updatedArticle: ArticleToUpdateModel) {
    return this.http.put('http://localhost:8080/Zenikademy/articles/' + id, updatedArticle);
  }

  updateArticleStatus(id: string, articleToPublish: ArticleToPublishModel) {
    return this.http.put('http://localhost:8080/Zenikademy/articles/publish/' + id, articleToPublish);
  }
}
