package com.jowan.evalfinaleback.domain.articleEnum;

public enum ArticleStatus {

    DRAFT("en attente de publication"),
    PUBLISHED("publié");

    private final String value;

    ArticleStatus(final String newValue){
        this.value = newValue;
    }

    public String getValue(){
        return value;
    }
}
