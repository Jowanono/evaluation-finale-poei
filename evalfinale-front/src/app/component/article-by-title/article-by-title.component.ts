import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article-by-title',
  templateUrl: './article-by-title.component.html',
  styleUrls: ['./article-by-title.component.css']
})
export class ArticleByTitleComponent implements OnInit {

  titleFromInput: string = null;
  // tslint:disable-next-line:no-inferrable-types
  isHidden: boolean = true;
  error: string;

  constructor() {
  }

  ngOnInit() {
  }

  change() {
    this.isHidden = true;
  }

  clicked() {
    this.isHidden = false;
  }

  onArticleError($event) {
    this.error = $event;
  }

}
