package com.jowan.evalfinaleback.repository;

import com.jowan.evalfinaleback.domain.Article;
import com.jowan.evalfinaleback.domain.articleEnum.ArticleCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ArticleRepository extends CrudRepository<Article, String> {

    List<Article> findAll();

    @Query("SELECT a FROM Article a where a.title =:title")
    List<Article> fetchArticlesByTitle(@Param("title") String title);

    @Query("SELECT a FROM Article a where a.category =:category")
    List<Article> fetchArticlesByCategory(@Param("category") ArticleCategory category);

}
