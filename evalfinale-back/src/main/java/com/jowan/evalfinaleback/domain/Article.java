package com.jowan.evalfinaleback.domain;

import com.jowan.evalfinaleback.domain.articleEnum.ArticleCategory;
import com.jowan.evalfinaleback.domain.articleEnum.ArticleStatus;
import org.apache.commons.text.similarity.LevenshteinDistance;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "articles")
public class Article {

    @Id
    private String id;
    private String title;
    private String content;
    private String synopsis;
    private ArticleStatus status;
    private ArticleCategory category;

    protected Article() {}

    public Article(String id, String title, String content, String synopsis, ArticleStatus status, ArticleCategory category) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.synopsis = synopsis;
        this.status = status;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public ArticleStatus getStatus() {
        return status;
    }

    public ArticleCategory getCategory() {
        return category;
    }
}
