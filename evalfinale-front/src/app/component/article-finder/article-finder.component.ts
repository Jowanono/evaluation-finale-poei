import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article-finder',
  templateUrl: './article-finder.component.html',
  styleUrls: ['./article-finder.component.css']
})
export class ArticleFinderComponent implements OnInit {

  idFromInput: string;
  // tslint:disable-next-line:no-inferrable-types
  isHidden: boolean = true;
  error: string;

  constructor() {
  }

  ngOnInit() {
  }

  change() {
    this.isHidden = true;
  }

  clicked() {
    this.isHidden = false;
  }

  onArticleError($event) {
    this.error = $event;
  }


}
