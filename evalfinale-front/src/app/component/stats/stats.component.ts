import {Component, OnChanges, OnInit} from '@angular/core';
import {ArticleModel} from '../../model/article.model';
import {UserModel} from '../../model/user.model';
import {ZenikademyService} from '../../service/zenikademy.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit, OnChanges {

  articles: ArticleModel [] = [];
  users: UserModel [] = [];
  articleNumber: number;
  userNumber: number;

  constructor(private service: ZenikademyService) {
  }

  ngOnInit() {
    this.service.getArticleList().subscribe(articles => {
      this.articles = articles;
    });
    this.service.getUserList().subscribe(users => {
      this.users = users;
    });
    this.getArticleNumber();
    this.getUserNumber();
  }

  ngOnChanges() {
    this.getArticleNumber();
    this.getUserNumber();
  }

  getArticleNumber(): number {
    this.articleNumber = this.articles.length;
    return this.articleNumber;
  }

  getArticleNumberByCategory(category: string): number {
    return this.articles.filter(a => a.category === category).length;
  }

  getUserNumber(): number {
    this.userNumber = this.users.length;
    return this.userNumber;
  }

}
